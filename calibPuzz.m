classdef calibPuzz < PTBdisp
    % calibration puzzle.  initially cuts an image into fragments (frag)
    % and displays frags in different areas on screen.  animates each frag
    % to correct orientation in center of screen
    
    properties
        fragPos         % [4 x numFrag] PTB pos of each frag
        fragPosFinal    % *[4 x numFrag] PTB pos of the final 
        %                   position of each Frag
        animTime        % [scalar]   time in seconds for each animation
        fragPileIdx     % [1 x numFrag] Pile Index for each fragment pointer 
    end            
    properties (Dependent = true)
        numImagePerPile
    end
    properties (Hidden = true)
        fragTexturePointers     %texture pointers to the image fragments
        image                   %rgb image map
        fragPosFinalOrder       %*[4 x numFrag] PTB of the final position
        %                       for each frag 
        %                       *NOTE: the difference between fragPosFnal
        %                       and fragPosFinalORder is that fragPosFinal
        %                       is the final positions based off of
        %                       randomized texture pointers,
        %                       fragPosFinalOrder, is the position of the
        %                       textures from left to right
    end
    
    methods
        function self = calibPuzz(imagePath, ...
                fragPilePosRatio, ...
                fragPileMinFrag, ...
                varargin)
            %constructor
            %inputs
            %   imagePath -     string, image file (assumed on matlab path)
            %   fragPilePosRatio -  [4 x numFragPile] PTB posRatio of
            %                       initial piles of frag
            %   fragPileMinFrag - [numFragPile x 1] scalar, minimum number
            %                     of frag in each pile
            
            
            self = self@PTBdisp(varargin{:});
            
            %finds the x and y pixel pos of the center of the screen
            screenCenterXY= ...
                [((self.pos(3) - self.pos(1)) / 2) + self.pos(1), ...
                ((self.pos(4) - self.pos(2)) / 2) + self.pos(2)];
            fragMinNum = sum(fragPileMinFrag);
            
            p = inputParser;
            p.KeepUnmatched = true;
            
            p.addRequired('imagePath');
            p.addRequired('fragPilePosRatio', ...
                @(x)(size(x,2) == 4));
            p.addRequired('fragPileMinFrag');
            p.addParameter('screen2ImageRatio', 2/3)
            p.addParameter('imageCenter', screenCenterXY); %Will Break
            p.addParameter('animTime', .5);
            p.parse(imagePath, fragPilePosRatio, fragPileMinFrag, ...
                varargin{:});
            
            fragPilePosRatio = p.Results.fragPilePosRatio;
            fragPileMinFrag = p.Results.fragPileMinFrag;
            
            %sets animeTime property to the parse result of animeTime
            self.animTime = p.Results.animTime;
            
            % find size of image file and Screen Center
            self.image = imread(p.Results.imagePath);
            imageSize = size(self.image(:,:,1))';
            
            %Set Image rescale Size
            scaleRatio(1:2) = ...
                (p.Results.screen2ImageRatio .* ...
                (self.pos(3:4) - self.pos(1:2))) ./ imageSize;
            
            %Rescales Image
            self.image = imresize(self.image, min(scaleRatio));
            imageSize = size(self.image(:,:,1));
            
            
            %Create Icon box to find positions of the fragments, 
            %**These positions will have gaps between each fragment
            %   depending on diference between image actual size and 
            %   numrows/cols * fragSize
            iconBoxObj = ...
                iconBox( ...
                'windowPointer', self.windowPointer, ...
                'pos',...
                [p.Results.imageCenter(1) - (imageSize(2) / 2), ...
                p.Results.imageCenter(2) - (imageSize(1) / 2), ...
                p.Results.imageCenter(1) + (imageSize(2) / 2), ...
                p.Results.imageCenter(2) + (imageSize(1) / 2)]');

            iconBoxObj.ComputeIconPos('numIcons', fragMinNum)
            
            %Create new IconBox to close Gaps in fragment positions
            IconBoxObjNewBorders= ... 
                [min(iconBoxObj.iconStruct.pos(1)), ...
                min(iconBoxObj.iconStruct.pos(2)), ...
                min(iconBoxObj.iconStruct.pos(1)) +  ...
                (iconBoxObj.dimIcon(1) * iconBoxObj.iconStruct.sizePix(1)) ...
                min(iconBoxObj.iconStruct.pos(2)) +  ...
                (iconBoxObj.dimIcon(2) * iconBoxObj.iconStruct.sizePix(2))];
            
            %Update Icon Box Obj to close gaps
                iconBoxObj = ...
                iconBox( ...
                'windowPointer', self.windowPointer, ...
                'pos', IconBoxObjNewBorders', ...
                'dimIcon', iconBoxObj.dimIcon );
            
            % compute fragPosFinal
            self.fragPosFinalOrder = iconBoxObj.iconStruct.pos;
            
            %initializes placeing fragments into frag piles
            self.ResetFrag( ...
                fragPilePosRatio, ...
                fragPileMinFrag);
        end
        
        function Draw(self)
            % draws all icons in their current position
            Screen('DrawTextures', ...
                self.windowPointer, ...
                self.fragTexturePointers, ...
                [], ...% sourceRect
                self.fragPos);
        end
        
        function ResetFrag(self, fragPilePosRatio, ...
                fragPileMinFrag, varargin)
            
            % place image fragments into fragPiles arbitrarily
            % Inputs
            %    imagePath        image being split
            %    fragPilePosRatio    [numPile x 4] Positions of frag piles
            %    fragPileMinFrag     [numPile x 1] min amount of Frags in a
            %                            Pile
            
            %Parse Builder
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('fragPilePosRatio')
            p.addRequired('fragPileMinFrag');
            p.parse(fragPilePosRatio, fragPileMinFrag, ...
                varargin{:});
            
            minFrags = p.Results.fragPileMinFrag;
            numPiles = length(p.Results.fragPileMinFrag);
            
            
            %Frag rect is a temporary variable used to find the position of
            %each fragment being made
            fragSize = self.fragPosFinalOrder(4, 1) - ...
                self.fragPosFinalOrder(2, 1);
            fragRect = [1, 1, fragSize, fragSize];
            
            numFrag = size(self.fragPosFinalOrder, 2);
            
            %image size is the pixel size of the user inputed image
            imageSize = ...
                [self.fragPosFinalOrder(4,end) - self.fragPosFinalOrder(2,1) ...
                self.fragPosFinalOrder(3,end) - self.fragPosFinalOrder(1,1)];
            
            %image Frag and FragIdx are used to identify the current
            %fragment being created, and save its position to a cell array
            imageFrag(1:numFrag) = {nan};
            fragIdx = 1;
            
            %breaks images up into fragments changeing x pos then y pos.
            while fragRect(3) <= imageSize(1)
                %resets to leftmost fragment at new y position
                fragRect(2:2:4) = [1, fragSize];
                while fragRect(4) <= imageSize(2)
                    %sets fragment to new x position
                    imageFrag{fragIdx} = ...
                        self.image(fragRect(1) : fragRect(3), ...
                        fragRect(2) : fragRect(4),:);
                    
                    fragRect(2:2:4) = fragRect(2:2:4) + fragSize;
                    
                    fragIdx = fragIdx +1;
                end
                
                fragRect(1:2:3) = fragRect(1:2:3) + fragSize;       
            end
            
            %Builds texture Pointers
            self.fragTexturePointers = self.BuildTextures(...
                'images', imageFrag);
                        
            %creates the self.fragPileIdx and distributes
            %minimum amount of fragments into desagnated piles
            for pileIdx = 1:length(minFrags)
                if pileIdx == 1
                    self.fragPileIdx(1:minFrags(pileIdx)) =  ...
                        ones(minFrags(pileIdx),1) * pileIdx;
                else
                    self.fragPileIdx( ...
                        sum(minFrags(1:pileIdx - 1)) + 1 : ...
                        sum(minFrags(1:pileIdx))) =  ...
                        ones(minFrags(pileIdx),1) * pileIdx;
                end
            end
            
            %Distibutes the rest of the fragments by placeing excess in
            %pile 1, pile 2 , ... , pile n, pile 1, pile 2...
            pileDistIdx = 0;
            lastPartition = sum(minFrags);
            while length(self.fragPileIdx) < numFrag
                
                pileIdx = rem(pileDistIdx, numPiles) + 1;
                pileDistIdx = pileDistIdx +1;
                self.fragPileIdx(lastPartition +1) = pileIdx;
                lastPartition = lastPartition + 1;
                
            end
            
            self.fragPileIdx = ...
                self.fragPileIdx(randperm(length(self.fragPileIdx)));

            %Converts the Pile ratio position to pixel position
            pilePTBPos(:, 1:2:4) = ...
                floor(p.Results.fragPilePosRatio(:, 1:2:3) * ...
                (self.pos(3) - self.pos(1)));
            pilePTBPos(:, 2:2:4) =  ...
                floor(p.Results.fragPilePosRatio(:, 2:2:4) * ...
                (self.pos(4) - self.pos(2)));
            
            %Sets th fragment mixed position and indexes which fragment
            %has which position and which pile in the fragPiles var
            for pileIdx = 1 : numPiles
                numFrags = sum(self.fragPileIdx == pileIdx);
                fragIdxs = find(self.fragPileIdx == pileIdx);
                for idx = 1 : numFrags
                    
                    fragIdx = fragIdxs(idx);
                    
                    %If the fragment size is larger than the dimensions of
                    %the user specified pile box, the non-fitting dimension
                    %of the pile is set to the fragment size, and a warning
                    %is issued
                    %**NOTE: only changes right and lower bounds of pile,
                    %   if a pile overflows on the bottom left the
                    %   fragments will not show in full
                    if fragSize >= ...
                            (pilePTBPos(pileIdx, 3) - ...
                            pilePTBPos(pileIdx, 1))
                        pilePTBPos(pileIdx, 3) = ...
                            fragSize + pilePTBPos(pileIdx, 1) + 1;
                        warning(...
                            ['fragment pile was smaller than fragment' ...
                            'Size ,the size of the pile has been changed']);
                    end
                    if fragSize >= ...
                            (pilePTBPos(pileIdx, 4) -  ...
                            pilePTBPos(pileIdx, 2))
                        pilePTBPos(pileIdx, 4) = ...
                            fragSize + pilePTBPos(pileIdx, 2) + 1;
                        warning( ...
                            ['fragment pile was smaller than fragment' ...
                            'Size ,the size of the pile has been changed']);
                    end
                    %Puts fragment at random position inside of the
                    %fragPile without going of the the frag pile bounds
                    dispXY = [self.pos(1), self.pos(2)];
                    placeRangeMinXY = pilePTBPos(pileIdx, 1:2);
                    placeRangeMaxXY = pilePTBPos(pileIdx, 3:4) - fragSize;
                    
                    self.fragPos(1:2, fragIdx) = ...
                        [randi([placeRangeMinXY(1), ...
                        placeRangeMaxXY(1)]) + dispXY(1) ...
                        randi([placeRangeMinXY(2), ...
                        placeRangeMaxXY(2)]) + dispXY(2)];
                    
                    self.fragPos(3:4, fragIdx) = ...
                        self.fragPos(1:2, fragIdx) + fragSize;
                end
            end
            
            
            %saves the final picture order 
            self.fragPosFinal = self.fragPosFinalOrder;
        end
        function numImagePerPile = get.numImagePerPile(self)
            for pileIdx = length(self.fragPileIdx):-1:1
                numImagePerPile(pileIdx) = ...
                    length(find(self.fragPileIdx==pileIdx));
            end
        end
        function Animate(self, pileIdx, varargin)
            % Animates a single image fragment to its final position
            % Inputs
            %   animTime   time to Animate icon movement
            %   pileIdx    selects pile to animate the top frag
            
            %input parser
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('pileIdx');
            p.addParameter('timeSec', self.animTime);
            p.addParameter('backgroundColor', [0 0 0]);
            
            p.parse(pileIdx, varargin{:});
            
            pileIdx = p.Results.pileIdx;

            %create index to the fragment moved
            fragIdx = ...
                find(self.fragPileIdx == pileIdx, 1, 'Last');
            if isempty(fragIdx)
                error('no fragments in this pile left to animate');
            end
            
            %sets number frames based on animation time and screen refresh
            %rate
            numFrames = ceil(p.Results.timeSec * 60);
            
            %get increment between first and final positions
            posDiff = ...
                self.fragPosFinal(:, fragIdx) - self.fragPos(:,fragIdx);
            posInc = posDiff / numFrames;
            
           %draw textures
                Screen('DrawTextures', self.windowPointer, ...
                    self.fragTexturePointers, [], self.fragPos);
                
            %draw textures in frames changeing the position at rate of
            %1/numFrames per s
            for frameIdx = 1:numFrames
                %clear screen Black
                Screen('FillRect', self.windowPointer, ...
                    p.Results.backgroundColor,self.pos);
                
                %change frag pos at frag idx to totalPosChange/numFrames
                %more
                self.fragPos(:, fragIdx) = ...
                    self.fragPos(:, fragIdx) + posInc;
               
                %draw textures
                Screen('DrawTextures', self.windowPointer, ...
                    fliplr(self.fragTexturePointers), [], self.fragPos);
                
                %flip screen
                Screen('Flip', self.windowPointer, [], 1);
            end
            %change fragIdx to NaN
            self.fragPileIdx(fragIdx) = 0;
        end

        function AnimateRest(self,varargin)
           % animate all icons to their final position (which haven't
           % already been animated)
           p = inputParser;
            p.KeepUnmatched = true;
            p.addParameter('timeSec', 2);
            p.parse(varargin{:});
           
            numIcons = sum(self.numImagePerPile);
            
            timePerFrag = p.Results.timeSec/numIcons;
           % animate
           for pileIdx = 1:length(self.numImagePerPile)
               for fragIdx = 1:self.numImagePerPile(pileIdx)
                   self.Animate(pileIdx, 'timeSec', timePerFrag)
               end
           end
        end   
    end
end